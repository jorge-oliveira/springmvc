<%--
  Created by IntelliJ IDEA.
  User: mr_mufy
  Date: 13/01/2018
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/my-test.css">

    <script src="${pageContext.request.contextPath}/resources/js/simple-test.js"></script>

    <title>SpringMVC </title>
</head>
<body>

<h2>Bem vindo à pagina inicial do Spring MVC - Home Page </h2>
<hr>

<a href="hello/showForm"> Hello World Form </a>

<br><br>

<a href="student/showForm"> Student Form </a>

<br><br>

<a href="customer/showForm"> Customer Form </a>


<hr>
<hr>
<!-- Defining Static requests -->
<a href="hello">Plain Hello World</a>

<br><br>

<img src="${pageContext.request.contextPath}/resources/images/spring-logo.png"/>

<br><br>

<input type="button" onclick="doSomeWork()" value="Click Me"/>


-- Teste --
${pageContext.request.contextPath}
</body>
</html>
