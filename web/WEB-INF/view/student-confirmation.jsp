<%--
  Created by IntelliJ IDEA.
  User: mr_mufy
  Date: 14/01/2018
  Time: 11:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Student - Confirmation Form</title>
</head>
<body>

<!-- retrieve the parameters send by the controller -->
the student is confirmed: ${student.firstName} ${student.lastName}
<br><br>
Country: ${student.country}
<br><br>
Favorite Language : ${student.favoriteLanguage}
<br><br>
<!-- importar a taglib prefix="c" no inicio da pagina -->
Operating Systems:
<ul>
    <c:forEach var="temp" items="${student.operationSystems}">

        <li> ${temp} </li>

    </c:forEach>
</ul>

</body>
</html>
