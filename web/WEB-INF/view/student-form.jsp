<%--
  Created by IntelliJ IDEA.
  User: mr_mufy
  Date: 14/01/2018
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Student - Form</title>
</head>
<body>

<form:form action="processForm" modelAttribute="student">
    First name: <form:input path="firstName"/>

    <br><br>

    Last name: <form:input path="lastName"/>

    <br><br>

    Country:
    <!-- On submit spring call setCountry() -->
    <form:select path="country">
        <!-- Spring will call student.getCountryOptions() -->
        <form:options items="${theCountryOptions}"/>
    </form:select>

    <br><br>

    Favorite Language:
    <!-- Spring will call student.setFavoriteLanguage() -->

    <br><br>

    <!-- Populateed from JAVA -->
    <form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguageOptions}"/>

    <br><br>

    Operrating Systems:
    Linux <form:checkbox path="operationSystems" value="Linux"/>
    Mac OS <form:checkbox path="operationSystems" value="Mac OS"/>
    Windows <form:checkbox path="operationSystems" value="Windows"/>


    <br><br>


    <input type="submit" value="Submit">

</form:form>


</body>
</html>
