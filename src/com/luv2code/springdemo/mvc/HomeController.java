package com.luv2code.springdemo.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/inicio") // se ficar apenas a barra / nao funciona o request
    public String showPage(){
        System.out.println("Dentro do metodo showPage()");
        return "main-menu";
    }
}
